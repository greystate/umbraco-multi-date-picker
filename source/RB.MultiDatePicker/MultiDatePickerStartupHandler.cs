﻿using System.Web.Routing;
using Umbraco.Core;

namespace RB.MultiDatePicker
{
    public class MultiDatePickerStartupHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            MultiDatePickerRouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}