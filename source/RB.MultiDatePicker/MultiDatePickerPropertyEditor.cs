﻿using ClientDependency.Core;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web.PropertyEditors;

namespace RB.MultiDatePicker
{
    [PropertyEditor("RB.MultiDatePicker", "RB: Multi Date Picker", "/App_Plugins/RB.MultiDatePicker/Resource/Editor.html", ValueType = "TEXT")]
    [PropertyEditorAsset(ClientDependencyType.Javascript, "/App_Plugins/RB.MultiDatePicker/Resource/Dialog.js")]
    [PropertyEditorAsset(ClientDependencyType.Javascript, "/App_Plugins/RB.MultiDatePicker/Resource/Editor.js")]
    [PropertyEditorAsset(ClientDependencyType.Css, "/App_Plugins/RB.MultiDatePicker/Resource/Editor.css")]
    public class MultiDatePickerPropertyEditor : PropertyEditor
    {
        protected override PreValueEditor CreatePreValueEditor()
        {
            return new MultiDatePickerPreValueEditor();
        }
    }
}